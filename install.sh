 #!/bin/bash

#title           :install.sh
#description     :
#author          :JChiabrando
#creation_date   :210223
#last_update     :210224
#version         :1.1
#usage           :sudo bash install.sh
#REF             :
#===============================================================================
#

set -e 

echo " "
echo "----------------"
echo "- Instlando... -"
echo "----------------"
echo " "
echo " "



OPT=0
while [ $OPT -eq 0 ]
do
  echo "Elija que sistema operativo utiliza"
  echo " [1] = MAC OS "
  echo " [2] = Linux "
  echo " "
  read OPT
  if [ $OPT = "1" ]; then
    brew install bash
  elif [ $OPT = "2" ]; then
  	sleep 1
  else
  	echo "opcion invalida"
  	sleep 1
  	OPT=0
  fi
done

mkdir -p /usr/local/gtt-cloud-run/tools
cp -r ../gtt-cloud_run/* /usr/local/gtt-cloud-run/

echo ' #!/bin/bash
bash /usr/local/gtt-cloud_run/gtt-cloud_run.sh
' > /usr/local/bin/gtt-cloud-run
chmod +x /usr/local/bin/gtt-cloud-run

echo " "
echo " "
echo -ne '########                   (33%)\r'
sleep 1.5
echo -ne '################           (66%)\r'
sleep 1.5
echo -ne '########################   (100%)\r'
echo -ne '\n'

echo "------------------------"
echo "- Instlancion exitosa. -"
echo "------------------------"
echo " "

exit 0
