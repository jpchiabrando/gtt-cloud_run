 #!/bin/bash

#title           :gtt-cloud_run.sh
#description     :
#author          :JChiabrando
#creation_date   :210223
#last_update     :210224
#version         :1.1
#usage           :sudo bash gtt-cloud_run.sh
#REF             :
#===============================================================================
#

#=====================
#Directorio de trabajo

cd /usr/local/gtt-cloud_run/
set -e
#==========
#Aclaracion

clear
echo -e "\e[37m-------------------------------------------------------\e[0m"
echo -e "\e[31mIMPORTANTE\e[0m"
echo -e "\e[37mEjecutar este script con permisos de administrador\e[0m"
echo -e "\e[37m-------------------------------------------------------\e[0m"
echo " "
echo " "
echo -e "\e[37m-------------------------------------------------------\e[0m"
echo -e "\e[31mIMPORTANTE\e[0m"
echo -e "\e[37mEs necesario tener instalada la herramientas SDK de GCP\e[0m"
echo -e "\e[1;34mhttps://cloud.google.com/sdk/docs/quickstart#mac\e[0m"
echo -e "\e[37m-------------------------------------------------------\e[0m"


#=========
#Opciones

echo " "
echo -e "$YELLOW---------------------------------------------$NORMAL"
echo -e " .:: $BOLD Selecione que desea hacer $NORMAL ::."
echo -e "$YELLOW---------------------------------------------$NORMAL"
echo " "
echo -e "Deploy a new webservices"'\n'"Deploy a new version of a webservices"'\n'"Upload a container image to GCP " > ./tmp/opciones

#Listando opciones
echo " "
c=0
while IFS= read -r line
do
  let c=$c+1
  echo "[ $c ] = $line"
done < ./tmp/opciones
echo "[ Ctrl + c ] = Salir"

rm ./tmp/opciones
#asignando variable y exportando a archivo de deploy
OPT=0 
while [ $OPT -eq 0 ]
do
  #Seleccion de opcion
  echo " "
  echo "Seleccione una opcion"
  read OPT
  if [ $OPT -eq 1 ]; then
  	sleep 1
  	clear
  	bash ./tools/cloud_run_new.sh
  	exit 0
  elif [ $OPT -eq 2 ]; then
  	sleep 1
  	clear
  	bash ./tools/cloud_run_new_version.sh
  	exit 0
  elif [ $OPT -eq 3 ]; then
  	sleep 1
  	clear
  	bash ./tools/cloud_run_upload_image.sh
  	exit 0
  else
  	echo "opcion invalida"
  	sleep 1
  	OPT=0
  fi
done

exit 0

