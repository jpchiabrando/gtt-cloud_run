 #!/bin/bash

#title           :user_check.sh
#description     :
#author          :JChiabrando
#creation_date   :210223
#last_update     :210224
#version         :1.2
#usage           :sudo bash user_check.sh
#REF             :
#===============================================================================
#

echo " "
echo -e "$YELLOW-----------------------------------------------------------$NORMAL"
echo -e " .:: $BOLD Verificacion de cuenta configurada en uso de GPC $NORMAL ::."
echo -e "$YELLOW-----------------------------------------------------------$NORMAL"
echo " "




gcloud config configurations list

#Validando eleccion
echo " "
echo "El usuario marcado con TRUE es con el que desea operar? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" != "s" ]; then
    echo " "
    echo -e "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script \n"
    echo -e "\e[37mEjecute el siguiente comando para cambiar de cuenta::\e[0m"
    echo -e "\e[1;34mgcloud config configurations activate  "ACCOUNT_NAME" \e[0m"
    echo " "
    echo " "
    exit 1
fi

exit 0
