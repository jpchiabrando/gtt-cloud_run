 #!/bin/bash

#title           :service_name.sh
#description     :
#author          :JChiabrando
#creation_date   :210223
#last_update     :210224
#version         :1.0
#usage           :sudo bash service_name.sh
#REF             :
#===============================================================================
#

#Titulo
echo " "
echo -e "$YELLOW---------------------------------------------$NORMAL"
echo -e " .:: $BOLD Nombre del webservice a deployar $NORMAL ::."
echo -e "$YELLOW---------------------------------------------$NORMAL"
echo " "


echo -e "Colocar el nombre del webservice, este debe coincidir con el nombre del repo en bitbucket \n"
read SERVICE

echo " "
echo "EL nombre del webservice sera::"
echo -e "\e[34m$SERVICE\e[0m"
echo " "


#Validando eleccion
echo "Es la opcion deseada? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" != "s" ]; then
    echo " "
    echo -e "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script \n"
    exit 1
fi

echo SERVICE=$SERVICE >> ./tmp/deploy_vars

exit 0