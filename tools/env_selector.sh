 #!/bin/bash

#title           :env_selector.sh
#description     :
#author          :JChiabrando
#creation_date   :210223
#last_update     :210224
#version         :1.1
#usage           :bash env_selector.sh
#REF             :
#===============================================================================
#


#Titulo
echo " "
echo -e "$YELLOW--------------------------------------------$NORMAL"
echo -e " .:: $BOLD Variables de Entorno necesarias $NORMAL ::."
echo -e "$YELLOW--------------------------------------------$NORMAL"
echo " "


#Selecinar archivo de variables de entorno
echo "Seleccionar ubicacion el archivo donde estan las variables de entorno declaradas"
read UBICACION

#Copiando variables de archivo de variables a archivo de deploy
cat $UBICACION > ./tmp/envs
cat tmp/envs | grep -e "#" -e "{" -e "}" -e "," > ./tmp/no_valid_env
cat tmp/envs | grep -v "#" | grep -v  "{" | grep -v  "}" | grep -v  "," > ./tmp/env_0

cat ./tmp/env_0 | sed '/^ *$/d' | tr '\n' ',' > env_1

sed -i 's/^/"/' env_1
sed -i 's/.$//g' env_1
sed -i 's/$/"/' env_1

ENV_VAR=$(cat env_1)

echo ENV_VAR=$ENV_VAR >> ./tmp/deploy_vars


#IMPORTANTE
#El archivo env se elimina en el sumary
rm env_*