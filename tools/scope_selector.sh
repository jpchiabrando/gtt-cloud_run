 #!/bin/bash

#title           :scope_selector.sh
#description     :
#author          :JChiabrando
#creation_date   :210223
#last_update     :210224
#version         :1.0
#usage           :sudo bash project_selector.sh
#REF             :
#===============================================================================
#
#Titulo
echo " "
echo -e "$YELLOW-------------------------------------------$NORMAL"
echo -e " .:: $BOLD Seleccion de Entorno de deploy $NORMAL ::."
echo -e "$YELLOW-------------------------------------------$NORMAL"
echo " "
echo -e TESTING'\n'PRODUCTION > entorno

#Listando opciones
echo " "
c=0
while IFS= read -r line
do
  let c=$c+1
  echo "[ $c ] = $line"
done < ./entorno

#Seleecion de entorno
echo " "
echo "Seleccion de entorno"
read OPT4

#asignando variable y exportando a archivo de deploy
SCOPE=$(awk "NR==$OPT4" entorno)


echo " "
echo "El entorno de deployment seleccionado es:"
echo -e "\e[34m$SCOPE\e[0m"
echo " "


#Validando eleccion
echo "Es la opcion deseada? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" != "s" ]; then
    echo " "
    echo -e "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script \n"
    exit 1
fi

SCOPE=$(echo ${SCOPE:0:4} |awk '{print tolower($0)}')
echo SCOPE=$SCOPE >> ./tmp/deploy_vars

rm entorno

exit 0