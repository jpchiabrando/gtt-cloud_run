 #!/bin/bash

#title           :summary.sh
#description     :
#author          :JChiabrando
#creation_date   :210223
#last_update     :210225
#version         :1.1
#usage           :sudo bash summary.sh
#REF             :
#===============================================================================
#


#-------
#Titulo
#-------
echo " "
echo -e "$YELLOW--------------------------------------$NORMAL"
echo -e " .:: $BOLD Resumen de implementacion $NORMAL ::."
echo -e "$YELLOW--------------------------------------$NORMAL"
echo " "

#Carga de variables
#source <(grep = ./tmp/deploy_vars | sed -e 's/\s*=\s*/=/g' -e 's/^;/#/g')

echo " "
echo -e "\e[32mCaracteristicas principales\e[0m"

echo " "
c=0
while IFS= read -r line
do
  let c=$c+1
  if [[ "$line" == *"ENV_VAR"* ]]; then
    sleep 0.1
  else
  echo -e "\e[1m$line\e[0m"
  echo " "
  fi
done < ./tmp/deploy_vars

echo " "
echo -e "\e[32mVariables de entorno\e[0m"
echo " "
c=0
while IFS= read -r line
do
  let c=$c+1
  echo -e "\e[1m$line\e[0m"
  echo " "
done < ./tmp/env_0

echo " "
echo -e "\e[31mVariables a invalidas no cargadas\e[0m"
echo " "
c=0
while IFS= read -r line
do
  let c=$c+1
  echo -e "\e[1m$line\e[0m"
  echo " "
done < ./tmp/no_valid_env

if [ $c -gt 1 ]; then
    echo " "
    echo -e "Consultar con el area de DevOPS para la carga de estas variable \n"
    echo " "
fi
echo " "
echo " "


#Validando resumen
echo " "
echo " "
echo -e "\e[31mIMPORTANTE:\e[0m\e[1mValidar los parametros del resumen\e[0m"
echo " " 
echo "Desea continuar con la implementacion? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" != "s" ]; then
    echo " "
    echo -e "Se cancelo la implementacion \n"
    exit 1
fi
echo " "
echo " "

rm ./tmp/env_0 ./tmp/envs

exit 0
