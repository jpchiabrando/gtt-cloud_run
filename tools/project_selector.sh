 #!/bin/bash

#title           :project_selector.sh
#description     :
#author          :JChiabrando
#creation_date   :210223
#last_update     :210224
#version         :1.0
#usage           :sudo bash project_selector.sh
#REF             :
#===============================================================================
#

#Titulo
echo " "
echo -e "$YELLOW--------------------------------------------$NORMAL"
echo -e " .:: $BOLD Eleccion de proyecto de trabajo $NORMAL ::."
echo -e "$YELLOW--------------------------------------------$NORMAL"
echo " "


#Listando los proyectos
gcloud projects list > projects &&  awk '{print $1}' projects | grep -v PROJECT_ID > project_id

c=0
while IFS= read -r line
do
  let c=$c+1
  echo "[ $c ] = $line"
done < project_id
let c=$c+1
echo "[ new ] = Nuevo proyecto"

#elegir opcion
echo " "
echo "Elija el numero de la opcion deseada o escriba new en caso de ser un nuevo proyecto presione. CRTL+C para salir"
read OPT1

#Creando nuevo proyecto
if [ "$OPT1" == "new" ]; then
    echo " "
    echo "Solicitar la crearcion del proyecto al area de DevOPS"
    echo " "  
    exit 1
#    read PROJECT_ID
#    gcloud projects create $PROJECT_ID
fi

#Si se elige un proyecto que existe
echo " "
echo "Eligio el proyecto:"
echo -e "\e[34m$(awk "NR==$OPT1" project_id)\e[0m"

#asignando variable y exportando a archivo de deploy
PROJECT_ID=$(awk "NR==$OPT1" project_id)
echo PROJECT_ID=$PROJECT_ID >> ./tmp/deploy_vars

#Validando eleccion
echo " "
echo "Es la opcion deseada? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" != "s" ]; then
    echo " "
    echo -e "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script \n"
    exit 1
fi

#seteando el proyecto en el que se va a trabajar
gcloud config set project $PROJECT_ID

rm project_id projects

exit 0