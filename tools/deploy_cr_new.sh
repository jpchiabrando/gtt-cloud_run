 #!/bin/bash

#title           :deploy_cr_new.sh
#description     :
#author          :JChiabrando
#creation_date   :210225
#last_update     :210326
#version         :1.5
#usage           :sudo bash deploy_cr_new.sh (TIPO) 
#REF             :
#===============================================================================
#

source <(grep = ./tmp/deploy_vars | sed -e 's/\s*=\s*/=/g' -e 's/^;/#/g')

#------------------
#Deploy
#------------------

#Seleccionar tipo de deploy
#opciones VER o NEW
TIPO=$1

echo " "
echo " "


if [ "$TIPO" = "NEW" ]; then
  echo -e "\e[1m------------------------------\e[0m"
  echo -e "\e[1m- Deploying a new webservice -\e[0m"
  echo -e "\e[1m------------------------------\e[0m"
  echo " "
  gcloud beta run deploy $WEBSERVICE_NAME --image $IMAGE --region $REGION --platform managed --$ACCESS  --update-env-vars=$ENV_VAR --$MAX_INSTANCES --tag=$TAG
else
  echo -e "\e[1m-----------------------------------------\e[0m"
  echo -e "\e[1m- Deploying a new version of webservice -\e[0m"
  echo -e "\e[1m-----------------------------------------\e[0m"
  echo " "
  gcloud beta run deploy $WEBSERVICE_NAME --image $IMAGE --region $REGION --platform managed --$ACCESS  --update-env-vars=$ENV_VAR --$MAX_INSTANCES --no-traffic --tag=$TAG
fi

exit 0
