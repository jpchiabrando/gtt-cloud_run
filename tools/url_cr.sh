 #!/bin/bash

#title           :url_cr.sh
#description     :
#author          :JChiabrando
#creation_date   :210301
#last_update     :210302
#version         :1.0
#usage           :sudo bash url_cr.sh
#REF             :
#===============================================================================
#

source <(grep = ./tmp/deploy_vars | sed -e 's/\s*=\s*/=/g' -e 's/^;/#/g')

echo $WEBSERVICE_NAME > url_1
sed 's/^/"/' url_1 > url_2
sed 's/$/"/' url_2 > url_1
WEBSERVICE=$(cat url_1)


#listando las url del servicio deployado recientemente
gcloud beta run services list --platform=managed --filter=$WEBSERVICE | grep -v "LAST DEPLOYED" > url_2

URL_WEBSERVICE=$(cat url_2 | awk '{ print $4 }')

echo URL_WEBSERVICE=$URL_WEBSERVICE>> ./tmp/deploy_vars

rm url_*

exit 0 