 #!/bin/bash

#title           :set_selector.sh
#description     :
#author          :JChiabrando
#creation_date   :210223
#last_update     :210225
#version         :1.1
#usage           :sudo bash set_selector.sh
#REF             :
#===============================================================================
#


#-------
#Titulo
#-------
echo " "
echo -e "$YELLOW-------------------------------------------------$NORMAL"
echo -e " .:: $BOLD Seteos de configuracion del servicio $NORMAL ::."
echo -e "$YELLOW-------------------------------------------------$NORMAL"
echo " "


#-----------------------
#Configuracion de puerto
#-----------------------
PORT="8080"
echo " "
echo -e "\e[1m------------------------- \e[0m"
echo -e "\e[1m-Configuracion de puerto- \e[0m"
echo -e "\e[1m------------------------- \e[0m"
echo " "
echo -e "\e[1mEl puerto que utilizara el webservice por defecto es el \e[31m8080\e[0m \e[1m[Cambiarlo solo si es necesario] \e[0m"
echo " " 

echo " "
echo "Desea utilizar el puerto por defecto? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" = "s" ]; then
    PORT="8080"
elif [ "$CONTINUAR" = "n" ]; then
    echo " "
    echo "Seleccionar puerto"
    read PORT
fi

echo PORT=$PORT >> ./tmp/deploy_vars
sleep 1

#--------------
#Autenticacion
#--------------
#--[no-]allow-unauthenticated
#--allow-unauthenticated

echo " "
echo " "
echo -e "\e[1m------------------------------------ \e[0m"
echo -e "\e[1m-Configuracion Autenticacion basica-\e[0m"
echo -e "\e[1m------------------------------------ \e[0m"
echo " "

echo -e "\e[1mPor defecto el acceso libre al webservice esta \e[31mBloqueado\e[0m"
echo " " 

ACCESS="[no-]allow-unauthenticated"

echo " "
echo "Desea cambiar Habilitar acceso libre al webservice? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" = "s" ]; then
    ACCESS="allow-unauthenticated"
fi

echo ACCESS=$ACCESS >> ./tmp/deploy_vars

echo " "
echo " "
echo -e "\e[1mEn caso de necesitar alguna configuracion particular \e[0m"
echo -e "\e[1mde Acceso al webservice debe solicitarla al area DevOPS \e[0m"
sleep 3

#----------------------
#Cantidad de Instancias
#----------------------

echo " "
echo " "
echo -e "\e[1m----------------------------------------- \e[0m"
echo -e "\e[1m-Configuracion de cantidad de Instancias-\e[0m"
echo -e "\e[1m----------------------------------------- \e[0m"
echo " "

echo -e "\e[1mInstancias Minimas por defecto= \e[31m0 \e[0m"
echo " "
echo -e "\e[1mInstancias Maximas por defecto= \e[31m5 \e[0m"
echo " " 

MIN_INSTANCES="0"
echo MIN_INSTANCES=$MIN_INSTANCES >> ./tmp/deploy_vars
MAX_INSTANCES="max-instances=5"
echo MAX_INSTANCES=$MAX_INSTANCES >> ./tmp/deploy_vars

echo " "
echo -e "\e[1mEn caso de necesitar alguna configuracion particular de cantidad minima  \e[0m"
echo -e "\e[1mo maxima de instancias debe solicitarla al area DevOPS \e[0m"
echo " "
echo " "
sleep 3

exit 0