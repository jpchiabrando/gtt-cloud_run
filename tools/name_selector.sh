 #!/bin/bash

#title           :name_selector.sh
#description     :
#author          :JChiabrando
#creation_date   :210225
#last_update     :210225
#version         :1.0
#usage           :sudo bash name_selector.sh
#REF             :
#===============================================================================
#

# #-----------------------------
# # Lectura de variables
# #-----------------------------
source <(grep = ./tmp/deploy_vars | sed -e 's/\s*=\s*/=/g' -e 's/^;/#/g')

SERVICE=$(echo $SERVICE | tr "/" "-" | tr "." "-" | tr "_" "-")

WEBSERVICE_NAME=$(echo $PROJECT_ID-$SERVICE-$SCOPE)
echo WEBSERVICE_NAME=$WEBSERVICE_NAME >> ./tmp/deploy_vars