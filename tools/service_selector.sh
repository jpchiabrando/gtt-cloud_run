# !/bin/bash

# title           :service_selector.sh
# description     :
# author          :JChiabrando
# creation_date   :210301
# last_update     :210302
# version         :1.0
# usage           :sudo service_selector.sh
# REF             :
# ===============================================================================


#Cargando variables
source <(grep = ./tmp/deploy_vars | sed -e 's/\s*=\s*/=/g' -e 's/^;/#/g')

#seteando 
touch serv1
rm serv1

echo " "
echo -e "$YELLOW---------------------------------------------------------$NORMAL"
echo -e " .:: $BOLD Seleccionar el WEBSERVICE que se actualizara $NORMAL ::."
echo -e "$YELLOW---------------------------------------------------------$NORMAL"
echo " "

#Listando los servicios

if [ "$REGION" = "unknow" ]; then
    echo " "
    gcloud run services list --platform=managed > serv1
else
    gcloud run services list --platform=managed  --region=$REGION > serv1
fi

#acomodando las listas
echo " "
echo " "
cat serv1 | head -1 | sed 's/^/     /' 

cat serv1 | grep -v DEPLOYED | sed 's/^.\{15\}//' | sed 's/^[[:space:]]*//' > serv2

c=0
while IFS= read -r line
do
  let c=$c+1
  echo "[ $c ] = $line" 
done < serv2

#elegir opcion de regiones
echo " "
echo "Elija el numero de la opcion deseada"
read OPT1

awk "NR==$OPT1" serv2 | tr ' ' '\n' | sed '/^ *$/d' > serv3

#asignando variable y exportando a archivo de deploy
WEBSERVICE_NAME=$(sed -n '1p' serv3)
if [ "$REGION" = "unknow" ]; then
  REGION=$(sed -n '2p' serv3)
  cat ./tmp/deploy_vars | grep -v REGION | sed '/^ *$/d' > ./tmp/deploy_vars2
  cat ./tmp/deploy_vars2 > ./tmp/deploy_vars
  echo REGION=$REGION >> ./tmp/deploy_vars
fi

echo " "
echo "Eligio el servicio:"
echo -e "\e[34m$WEBSERVICE_NAME\e[0m"

#Validando eleccion
echo " "
echo "Es la opcion deseada? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" != "s" ]; then
    echo " "
    echo -e "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script \n"
    exit 1
fi

echo WEBSERVICE_NAME=$WEBSERVICE_NAME >> ./tmp/deploy_vars

rm serv* regions_all ./tmp/deploy_vars2

exit 0