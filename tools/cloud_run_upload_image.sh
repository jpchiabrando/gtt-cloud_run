 #!/bin/bash

#title           :cloud_run_upload_image.sh
#description     :
#author          :JChiabrando
#creation_date   :210301
#last_update     :210302
#version         :1.0
#usage           :sudo cloud_run_upload_image.sh
#REF             :
#===============================================================================
#

#=================
#Seteos de trabajo
#=================
#Para salir en caso de error
set -e

#Archivo de variables 
touch ./tmp/deploy_vars
rm ./tmp/deploy_vars
touch ./tmp/deploy_vars

#==========
#Aclaracion
#==========

#Validando
echo -e "\e[37m--------------------------------------------------------------\e[0m"
echo -e "\e[31mIMPORTANTE\e[0m"
echo -e "\e[37mEs necesario configurar gcloud y docker para subir la imagenes\e[0m"
echo -e "\e[37mEJECUTAR:\e[0m"
echo -e "\e[1;34mgcloud auth configure-docker \e[0m"
echo -e "\e[37m--------------------------------------------------------------\e[0m"
echo " "
echo "Tiene ya configurado en su PC gcloud y docker? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" != "s" ]; then
    echo " "
    echo -e "\e[37mEjecutar el comando especificado anteriormente\e[0m"
    exit 1
fi

#------------------------
#chequeo de cuenta en uso
#------------------------
bash tools/user_check.sh

#---------------------------------------------------------------
#Chequeo de proyectos disponibles para el usuario y elegimos uno
#---------------------------------------------------------------
bash tools/project_selector.sh

#---------------------------------------------------------------
#build imagen a partir de dockerfile
#---------------------------------------------------------------
bash tools/build_img_docker.sh


#------------------------------------
#Implementacion
#------------------------------------

#Validando
echo " "
echo -e "\e[37m----------------------------------------------------------------\e[0m"
echo -e "\e[31mIMPORTANTE\e[0m"
echo -e "\e[37mPara implementar esta nueva imagen vuelta a ejecutar el comado  \e[0m"
echo -e "\e[1;34mgtt-cloud_run \e[0m"
echo -e "\e[37mSeleccionado la opcion de nuevo deployment o nueva version      \e[0m"
echo -e "\e[37my buscar la imagen recien subida en el proyecto correspondiente \e[0m"
echo -e "\e[37m----------------------------------------------------------------\e[0m"
echo " "
sleep 2

exit 0