 #!/bin/bash

#title           :build_img_docker.sh
#description     :
#author          :JChiabrando
#creation_date   :210302
#last_update     :210302
#version         :1.0
#usage           :sudo build_img_docker.sh
#REF             :
#===============================================================================
#

echo " "
echo -e "$YELLOW----------------------------------------------$NORMAL"
echo -e " .:: $BOLD Generando y subiendo imagen a GCP $NORMAL ::."
echo -e "$YELLOW----------------------------------------------$NORMAL"
echo " "

echo " "
echo -e "\e[1mSeleccionar nombre a colocar en la imagen\e[0m"
echo -e "Colocar el nombre del imagen, este debe coincidir con el nombre del repo en bitbucket \n"
read IMAGEN

echo " "
echo "EL nombre del webservice sera::"
echo -e "\e[34m$IMAGEN\e[0m"
echo " "


echo " "
echo -e "\e[1mColocar la version del webservices que se va a implementar\e[0m"
echo -e "\e[1mEste dato debe coincidir con el TAG del repo del webservice en Bitbucket\e[0m"
read TAG


IMAGE=$IMAGEN:$TAG


echo " "
echo "La imagen y tag seleccionada es:"
echo -e "\e[34m$IMAGE\e[0m"
echo " "


#Validando eleccion
echo "Es la opcion deseada? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" != "s" ]; then
    echo " "
    echo -e "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script \n"
    exit 1
fi

echo IMAGE=$IMAGE >> ./tmp/deploy_vars

echo -e "\e[1mSeleccionar el dockerfile (Colocando la carpeta y el archivos)\e[0m"
read UBICACION

source <(grep = ./tmp/deploy_vars | sed -e 's/\s*=\s*/=/g' -e 's/^;/#/g')
echo " "
echo -e "\e[37mGenerando la imagen\e[0m"
docker build -f $UBICACION . --tag gcr.io/$PROJECT_ID/$IMAGE

echo " "
echo -e "\e[37mSubiendo la imagen a GCP\e[0m"
docker push gcr.io/$PROJECT_ID/$IMAGE

exit 0