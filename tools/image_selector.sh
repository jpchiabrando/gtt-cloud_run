 #!/bin/bash

#title           :image_selector.sh
#description     :
#author          :JChiabrando
#creation_date   :210129
#last_update     :210129
#version         :1.0
#usage           :bash image_selector.sh
#REF             :
#===============================================================================
#

#Funciones
#Titulo

echo " "
echo -e "$YELLOW-----------------------------------$NORMAL"
echo -e " .:: $BOLD Seleccion de imagen a utilizar ::."
echo -e "$YELLOW-----------------------------------$NORMAL"
echo " "

#listando imagenes y versiones
echo " "
echo -e "\e[1mLista de imagenes del proyecto\e[0m"
echo " "

gcloud container images list | grep -v NAME > images 
sleep 1
echo " "
echo " "
c=0
while IFS= read -r line
do
  let c=$c+1
  echo "[ $c ] = $line"
  echo " "
done < ./images

#elegir opcion de regiones
echo " "
echo "Elija la imagen a utilizar"
read OPT3

SERVICE=$(awk "NR==$OPT3" images)
echo " "
echo "Eligio la imagen:"
echo -e "\e[34m$SERVICE\e[0m"

gcloud container images list-tags $SERVICE | grep -v DIGEST > tags

echo " "
echo -e "\e[1mLista de las versiones de la imagen\e[0m"
echo " "

c=0
while IFS= read -r line
do
  let c=$c+1
  echo "[ $c ] = $line"
  echo $line > tag$c
  cat tag$c | cut -d " " -f 2 >> tag
done < ./tags

echo " "
echo "Elija la version de la imagen a utilizar"
read OPT4

TAG=$(awk "NR==$OPT4" tag)

IMAGE=$SERVICE:$TAG


echo " "
echo "La imagen y tag seleccionada es:"
echo -e "\e[34m$IMAGE\e[0m"
echo " "

#Validando eleccion
echo "Es la opcion deseada? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" != "s" ]; then
    echo " "
    echo -e "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script \n"
    exit 1
fi

#asignando variable y exportando a archivo de deploy
SERVICE=$(echo $SERVICE | tr "/" "\n" | awk "NR==3")
echo SERVICE=$SERVICE>> ./tmp/deploy_vars
echo IMAGE=$IMAGE>> ./tmp/deploy_vars

rm images tag*