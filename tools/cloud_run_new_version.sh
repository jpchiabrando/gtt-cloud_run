 #!/bin/bash

#title           :deploy_cloud_run.sh
#description     :
#author          :JChiabrando
#creation_date   :210129
#last_update     :210224
#version         :1.5
#usage           :bash deploy_cloud_run.sh
#REF             :
#===============================================================================
#

#Titulo

echo -e "\e[37m--------------------------\e[0m"
echo -e "\e[31mDeployment a New Webservice\e[0m"
echo -e "\e[37m---------------------------\e[0m"


#Seteos de trabajo
set -e

touch ./tmp/deploy_vars
rm ./tmp/deploy_vars
touch ./tmp/deploy_vars

#------------------------
#chequeo de cuenta en uso
#------------------------
bash tools/user_check.sh


#---------------------------------------------------------------
#Chequeo de proyectos disponibles para el usuario y elegimos uno
#---------------------------------------------------------------
bash tools/project_selector.sh


#-----------------------------------------------
# #Listar regiones disponibles y seleccionar una
#-----------------------------------------------
bash tools/region_selector.sh VER

#---------------

#---------------------------
#Colocar Nombre a webservice
#---------------------------
bash tools/service_selector.sh


#---------------------------
#Colocar TAG-VERSION
#---------------------------
bash tools/tag_selector.sh

#-------------------------
#Selecionar imagen y tag
#-------------------------
bash tools/image_selector.sh


# #---------------------------------------------
# #Seleccion de SCOPE
# #---------------------------------------------
# bash tools/scope_selector.sh


#-----------------------------
#listar variables de entorno
#-----------------------------
bash tools/env_selector.sh


#-----------------------------
# Seteos de la aplicacion
#-----------------------------
bash ./tools/set_selector.sh


#--------
#Resumen
#--------
bash ./tools/summary.sh


#------------------
#Deploy desde cero
#------------------
bash ./tools/deploy_cr_new.sh VER


#------------------
#Guardando URL
#------------------
bash ./tools/url_cr.sh


#----------------------------
#Resumen de la implementacion
#----------------------------
bash ./tools/cr_ws_describe.sh


#------------------
#Manejando deploys
#------------------
bash ./tools/deploy_selector.sh


exit 0
