 #!/bin/bash

#title           :region_selector.sh
#description     :
#author          :JChiabrando
#creation_date   :210129
#last_update     :210129
#version         :1.0
#usage           :bash region_selector.sh nueva-version $TIPO
#REF             :
#===============================================================================
#

#Seleccionar tipo de deploy
#opciones VER o NEW
TIPO=$1

echo " "
echo -e "$YELLOW------------------------------$NORMAL"
echo -e " .:: $BOLD Elecion de region $NORMAL ::."
echo -e "$YELLOW------------------------------$NORMAL"
echo " "

echo " "
echo -e "\e[37mPor defecto se utilizara la region us-central1\e[0m"
echo " "
REGION=us-central1

echo " "
echo "Utilizara la zona por defecto? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" = "s" ]; then
    echo " "
    echo REGION=$REGION >> ./tmp/deploy_vars
    exit 0
fi

echo " "

#listando regiones 
gcloud run regions list | grep -v NAME | grep -v europe | grep -v australia | grep -v asia | grep -v northamerica > regions

c=0
while IFS= read -r line
do
  let c=$c+1
  echo "[ $c ] = $line"
done < regions

if [ "$TIPO" = "VER" ]; then
  let c=$c+1
  echo "[ $c ] = No sabe"
  cp regions regions_all
  echo "unknow" >> regions
fi


#elegir opcion de regiones
echo " "
echo "Elija el numero de la opcion deseada"
read OPT2

echo " "
echo "Eligio la region:"
echo -e "\e[34m$(awk "NR==$OPT2" regions)\e[0m"

#asignando variable y exportando a archivo de deploy
REGION=$(awk "NR==$OPT2" regions)

echo REGION=$REGION >> ./tmp/deploy_vars

#Validando eleccion
echo " "
echo "Es la opcion deseada? [s]/[n]"
read CONTINUAR
if [ "$CONTINUAR" != "s" ]; then
    echo " "
    echo -e "La opcion no es la deseada, para comenzar de nuevo ejecute nuevamente este script \n"
    exit 1
fi

rm regions 

exit 0