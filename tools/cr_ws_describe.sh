 #!/bin/bash

#title           :cr_ws_describe.sh
#description     :
#author          :JChiabrando
#creation_date   :210303
#last_update     :210303
#version         :1.0
#usage           :bash cr_ws_describe.sh
#REF             :
#===============================================================================
#

echo -e "\e[1m----------------------\e[0m"
echo -e "\e[1m- Deployment details -\e[0m"
echo -e "\e[1m----------------------\e[0m"


#Cargando variables
source <(grep = ./tmp/deploy_vars | sed -e 's/\s*=\s*/=/g' -e 's/^;/#/g')


#Recopilando la info
echo " "
gcloud run  services describe $WEBSERVICE_NAME --project $PROJECT_ID --platform managed --region $REGION
echo " "
sleep 3


