 #!/bin/bash

#title           :tag_selector.sh
#description     :
#author          :JChiabrando
#creation_date   :210225
#last_update     :210225
#version         :1.0
#usage           :sudo bash tag_selector.sh
#REF             :
#===============================================================================
#

#--------------
#TAG - Version
#--------------

#-------
#Titulo
#-------
echo " "
echo -e "$YELLOW-------------------------------------------------$NORMAL"
echo -e " .:: $BOLD Configuracion de TAG/Version $NORMAL ::."
echo -e "$YELLOW-------------------------------------------------$NORMAL"
echo " "

#elegir opcion
echo " "
echo "Colocar la version del webservices que se va a implementar"
echo "Este dato debe coincidir con el TAG del repo del webservice en Bitbucket"
read TAG

echo TAG=$TAG >> ./tmp/deploy_vars

#--tag=version
